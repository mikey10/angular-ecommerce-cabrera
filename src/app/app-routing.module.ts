import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';
import { ProductosComponent } from './components/productos/productos.component';

// const routes: Routes = [{path:'auth', 
//                         loadChildren:()=>import('./components/auth/auth.module').then(m=>m.AuthModule)},
//                         {path: 'products', component: ProductosComponent},
//                         {path:'cart', component: CartComponent},
//                         {path:'', redirectTo: 'products', pathMatch:'full'},
//                         ];
const routes: Routes = [{path:'auth', 
                         loadChildren:()=>import('./components/auth/auth.module').then(m=>m.AuthModule)},
//                         {path: 'products', component: ProductosComponent},
//                         {path:'cart', component: CartComponent},
                         {path:'', redirectTo: 'auth', pathMatch:'full'},
                       ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
