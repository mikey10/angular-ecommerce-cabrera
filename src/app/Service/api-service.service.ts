import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { products } from '../interfaces/products';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  products!:products;
  constructor(private httpClient:HttpClient) { }

  getProduct() : Observable<products>{
    return this.httpClient.get<products>("https://fakestoreapi.com/products")
    .pipe(map((res:any)=>{
      return res;
    }))
  }
}
