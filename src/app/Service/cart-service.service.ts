import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartServiceService {

  public cartItemList : any =[];
  public cartArrayTemp : any =[];
  public productList = new BehaviorSubject<any>([]);
  public search = new BehaviorSubject<string>("");

  constructor() { }

  getProducts(){
    return this.productList.asObservable();
  }

  setProducts(product:any){
    this.cartItemList.push(product);
    this.productList.next(product);

  }

  addtoCart(product:any){
    this.cartItemList.push(product);
    this.cartItemList = [... new Set(this.cartItemList)];
    this.productList.next(this.cartItemList);
    this.getTotalPrice();
  }

  getProductos(): Observable<any>{
    const result = new Observable(obs=>{
      obs.next(this.cartItemList);
    })
    return result;
 }


  getTotalPrice(){
    let granTotal = 0;
    this.cartItemList.map((a:any, index:any)=>{
      granTotal = a.total;
    })
    return granTotal;
  }
   
  removeCartItem(product: any){
    this.cartItemList.map((a:any, index:any)=>{    
      if(product.id=== a.id){
        this.cartItemList.splice(index,1);
      }
    })
    this.productList.next(this.cartItemList);
  }
  removeAllCart(){
    this.cartItemList = []
    this.productList.next(this.cartItemList);
  }

}
