import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  public form!:FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
       this.form = this.formBuilder.group({
         correo: new FormControl('', Validators.required),
         password: new FormControl('', Validators.required)
       })
  }

  login(){
      this.router.navigate(['/header'])
  }

}
