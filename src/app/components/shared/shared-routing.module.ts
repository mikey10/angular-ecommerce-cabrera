import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from '../cart/cart.component';
import { HeaderComponent } from '../header/header.component';
import { ProductosComponent } from '../productos/productos.component';

const routes: Routes = [{path:'header', component: HeaderComponent},
                        {path:'products', component:ProductosComponent},
                        {path:'header/cart', component:CartComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
