import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { HeaderComponent } from '../header/header.component';
import { ProductosComponent } from '../productos/productos.component';
import { ProductosModule } from '../productos/productos.module';
import { CartComponent } from '../cart/cart.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [HeaderComponent, ProductosComponent, CartComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    ProductosModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    NgxImageZoomModule,
    
  ]
})
export class SharedModule { }
