import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiServiceService } from 'src/app/Service/api-service.service';
import { CartServiceService } from 'src/app/Service/cart-service.service';
import {MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
 
  public productLis !:any[];
  public filterCategory:any;
  config: any;
  searchKey:string = "";
  pageSize =1;
  images !: any;
  @ViewChild(MatPaginator)paginator!: MatPaginator

  constructor(private api: ApiServiceService,
              private cartService: CartServiceService,
              private spinner:NgxSpinnerService,
              private router:Router) { }

  ngOnInit(): void {
    this.images = "https://wittlock.github.io/ngx-image-zoom/assets/thumb.jpg";

    this.spinner.show();
     setTimeout(() => {
       this.spinner.hide();
       this.api.getProduct().subscribe(res=>{
        this.spinner.hide();
        // this.productLis = res;
        this.filterCategory = res;
      })
     }, 1000)
     
    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: 20
    };

    this.cartService.search.subscribe((val:any)=>{
      this.searchKey = val;
    })
  }


  addtoCart(item:any){
     this.cartService.addtoCart(item);
  }

   filter(category:string){
     this.filterCategory = this.productLis
     .filter((a:any)=>{
       if(a.category == category || category==''){
         return a;
       }
     })
   }

   pageChanged(event:any){
     this.config.currentPage = event;
   }

}
