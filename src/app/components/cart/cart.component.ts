import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CartServiceService } from 'src/app/Service/cart-service.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

   products : any =[];
   grandTotal!:number;
   total : any =[];
;

  constructor(private cartService: CartServiceService,
              private spinner:NgxSpinnerService,
              private router: Router) { }

  ngOnInit(): void {
    // this.cartService.getProductos().subscribe(res=>{
    //   console.log('productos',res);
    //   this.products = res;
    //   this.products.map((res:any)=>{
    //         this.grandTotal+res.price;
    //   })
    // })
    this.cartService.getProducts().subscribe(res=>{
      this.products = res;
      this.products = [... new Set(this.products)];
      this.grandTotal = this.cartService.getTotalPrice();
    })
    console.log(this.products);
  }

  removeItem(item: any){
    this.cartService.removeCartItem(item);
  }
  emptycart(){
    this.cartService.removeAllCart();
  }

}
