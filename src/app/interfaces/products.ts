export interface products {
    category: string;
    description:string,
    id:number,
    image:string,
    price:number,
    title:string
}