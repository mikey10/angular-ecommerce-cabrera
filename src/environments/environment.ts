// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA3xGQoNYcbcR7J98ddOrRI6CpNJPnu-d4",
  authDomain: "angular-app-a5e5b.firebaseapp.com",
  databaseURL: "https://angular-app-a5e5b.firebaseio.com",
  projectId: "angular-app-a5e5b",
  storageBucket: "angular-app-a5e5b.appspot.com",
  messagingSenderId: "113924405231",
  appId: "1:113924405231:web:f222d5791adf5ffc34e81a",
  measurementId: "G-6MZE48FR9Y"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
